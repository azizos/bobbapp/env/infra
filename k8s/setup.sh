#!/bin/bash
REPO_URL=$1
REPO_ID=$2
GITLAB_PRIVATE_TOKEN=$3
SEALEDSECRETS_SECRET=$4

## Check if the cluster exists
CLUSTER_STATUS=$(gcloud container clusters describe $TF_VAR_cluster_name --region $TF_VAR_region --format="value(status)")
if [[ $CLUSTER_STATUS != "RUNNING" ]]; then echo "Error: cluster is not running.." && exit 1; fi

## Get authenticated to the cluster and save kubeconfig
gcloud container clusters get-credentials $TF_VAR_cluster_name --region $TF_VAR_region --project=$TF_VAR_project

## Setup Helm repositories used to install Flux and SealedSecrets
helm repo add fluxcd https://charts.fluxcd.io
helm repo add stable https://kubernetes-charts.storage.googleapis.com/
helm repo update

## Install Flux

kubectl create ns flux

helm upgrade -i flux fluxcd/flux --wait \
    --namespace flux \
    --set git.url=$REPO_URL \
    --set git.ssh.secretName=flux-git-deploy \
    --set git.branch="master" \
    --debug

## Upload the public key of Flux to GitLab as deploy key

FLUX_PUBLIC_KEY=$(fluxctl identity --k8s-fwd-ns flux)
EXISTING_DEPLOY_KEY_ID=$(curl --header "PRIVATE-TOKEN: $GITLAB_PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/$REPO_ID/deploy_keys" | jq --exit-status '.[] | select(.title == "flux") .id')
RESULT=$?

if [ $RESULT -eq 0 ]; then
    curl --request DELETE --header "PRIVATE-TOKEN: $GITLAB_PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/$REPO_ID/deploy_keys/$EXISTING_DEPLOY_KEY_ID"
fi

curl --request POST \
    --header "PRIVATE-TOKEN: $GITLAB_PRIVATE_TOKEN" \
    --header "Content-Type: application/json" \
    --data "{\"title\":\"flux\", \"key\":\"$FLUX_PUBLIC_KEY\",  \"can_push\":\"true\"}" "https://gitlab.com/api/v4/projects/$REPO_ID/deploy_keys/"

## Install Helm operator

kubectl apply -f https://raw.githubusercontent.com/fluxcd/helm-operator/master/deploy/crds.yaml

helm upgrade -i helm-operator fluxcd/helm-operator --wait \
    --namespace flux \
    --set git.ssh.secretName=flux-git-deploy \
    --set helm.versions=v3 \
    --set git.pollInterval=1m \
    --debug

## Install and configure SealedSecrets package
echo $SEALEDSECRETS_SECRET | base64 -d >./sealed-secrets-key.yaml
kubectl apply -f ./sealed-secrets-key.yaml # to be used by the SealedSecrets controller
kubectl apply -f https://github.com/bitnami-labs/sealed-secrets/releases/download/v0.12.4/controller.yaml