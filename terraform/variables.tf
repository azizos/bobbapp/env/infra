variable "project" {}
variable "region" {}
variable "cluster_name" {}

variable "gcp_credentials" {
  default = "secrets/gcp_credentials.json"
}
