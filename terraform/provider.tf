terraform {
  required_version = "0.12.23"
}

provider "google" {
  version     = "3.12.0"
  credentials = file(var.gcp_credentials)
  project     = var.project
  region      = var.region
}