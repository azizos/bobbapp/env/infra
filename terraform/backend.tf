terraform {
 backend "gcs" {
   bucket  = "bobbapp-terraform-state"
   prefix  = "demo"
  credentials = "secrets/gcp_credentials.json"
 }
}